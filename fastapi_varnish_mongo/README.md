# Fast Api varnish mongo Back

## Dependencies

- Docker
- Docker Compose

## Startup

```bash
docker-compose up --build -d # --build is only needed the first time
                             # or after adding new dependencies to python
```

## Dump and restore database

### Dump

```bash
docker exec -it mongo bash -c 'mongodump --authenticationDatabase=admin --db "$MONGO_INITDB_DATABASE" --username "$MONGO_INITDB_ROOT_USERNAME" --password "$MONGO_INITDB_ROOT_PASSWORD"'
``` 

### Restore

```bash
docker exec -it mongo bash -c 'mongorestore --authenticationDatabase=admin -d "$MONGO_INITDB_DATABASE" --username "$MONGO_INITDB_ROOT_USERNAME" --password "$MONGO_INITDB_ROOT_PASSWORD" /dump/"$MONGO_INITDB_DATABASE"'
```

## Stop

```bash
docker-compose stop
```

## Remove

```bash
docker-compose down
```