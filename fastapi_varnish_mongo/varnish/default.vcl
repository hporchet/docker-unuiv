# specify the VCL syntax version to use
vcl 4.1;

import std;

backend default {
    .host = "api";
    .port = "8000";
	.probe = {
        .request =
            "HEAD /stats/healthcheck HTTP/1.1"
            "Host: localhost"
            "Connection: close"
            "User-Agent: Varnish Health Probe";
        .interval  = 5s;
        .timeout   = 5s;
        .window    = 5;
        .threshold = 2;
    }
    .connect_timeout        = 5s;
    .first_byte_timeout     = 90s;
    .between_bytes_timeout  = 2s;
}

acl purge {
	"localhost";
	"127.0.0.1";
	"::1";
	"api";
	"172.31.0.1";
}

sub vcl_backend_response {
    if (!beresp.http.X-Cache-Control ~ "cacheable") {
		set beresp.uncacheable = true;
		return(deliver);
	}
}


sub vcl_recv {
	if (req.http.Cache-Control ~ "(private|no-cache|no-store)" || req.http.Pragma == "no-cache") { return (pass); }

	if (req.method == "BAN") {
		if (!client.ip ~ purge) {
			return (synth(405, client.ip + " is not allowed to send BAN requests."));
		}
		ban("req.url ~ " + req.url);
		return (synth(200, "Banned " + req.url));
	}

    if (req.method == "BANMAIN") {
        ban("req.url ~ /");
		return (synth(200, "Banned"));
    }

	if (req.method == "PURGE") {
        if (!client.ip ~ purge) {
            return (synth(405, client.ip + " is not allowed to send PURGE requests."));
        }
        return (purge);
    }
}

sub vcl_hit {
	set req.http.x-cache = "hit";
	if (obj.ttl <= 0s && obj.grace > 0s) {
		set req.http.x-cache = "hit graced";
	}
}

sub vcl_miss {
	set req.http.x-cache = "miss";
}

sub vcl_pass {
	set req.http.x-cache = "pass";
}

sub vcl_deliver {
	unset resp.http.X-Cache-Control;
	if (obj.uncacheable) {
		set req.http.x-cache = req.http.x-cache + " uncacheable" ;
	} else {
		set req.http.x-cache = req.http.x-cache + " cached" ;
	}
	# uncomment the following line to show the information in the response
	set resp.http.x-cache = req.http.x-cache;
}

sub vcl_backend_error {
    set beresp.http.Content-Type = "text/html; charset=utf-8";
    synthetic(std.fileread("/etc/varnish/503.html"));
    return(deliver);
}