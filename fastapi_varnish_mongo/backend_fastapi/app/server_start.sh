#!/usr/bin/env bash

# Start the server
# if the environment variable DEBUG_MODE is set to true, then start the server in debug mode
# otherwise, start the server in production mode
if [ "$DEBUG_MODE" = "true" ]
then
    uvicorn main:app --reload --host "$API_HOST" --port "$API_PORT"
else
    uvicorn main:app --host "$API_HOST" --port "$API_PORT"
fi