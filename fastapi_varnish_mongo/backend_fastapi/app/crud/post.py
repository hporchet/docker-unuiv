from typing import List

from db.mongodb import AsyncIOMotorClient
from models.post import PostBase
from core.config import settings


async def get_all_posts(
        conn: AsyncIOMotorClient
) -> List[PostBase]:
    rows = conn[settings.database_name][settings.post_collection].find()
    return [PostBase(**row) async for row in rows]
