import logging

from motor.motor_asyncio import AsyncIOMotorClient
from core.config import settings
from .mongodb import db

logger = logging.getLogger(__name__)


async def connect_to_mongo():
    logger.info("Connecting to database...")
    db.client = AsyncIOMotorClient(str(settings.MONGO_URL),
                                   maxPoolSize=settings.MAX_CONNECTIONS_COUNT,
                                   minPoolSize=settings.MIN_CONNECTIONS_COUNT)
    logger.info("Database connected !")


async def close_mongo_connection():
    logger.info("Closing database connection...")
    db.client.close()
    logger.info("Database closed !")
