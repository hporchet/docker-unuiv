from pydantic import BaseConfig, BaseModel
from bson import ObjectId


class RWModel(BaseModel):
    class Config(BaseConfig):
        allow_population_by_field_name = True
        json_encoders = {ObjectId: str}
