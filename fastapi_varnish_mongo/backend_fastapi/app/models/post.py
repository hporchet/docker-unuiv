from pydantic import Field
from datetime import datetime, date
from typing import List, Dict
from .dbmodel import DBModelMixin
from .rwmodel import RWModel


class PostBase(RWModel):
    post_id: str = Field(...)
    user_id: str = Field(...)
    location_id: str = Field(...)
    comments: List[str] = Field(...)
    img_url: str = Field(...)
    description: str = Field(...)
    hashtags: List[str] = Field(...)
    number_of_likes: Dict[date, int] = Field(...)
    number_of_comments: Dict[date, int] = Field(...)
    timestamp: datetime = Field(...)
    sponsored: bool = Field(...)
    paid_partnership: bool = Field(...)


class PostInDB(DBModelMixin, PostBase):
    pass


class PostResponse(PostBase):
    post: PostBase


class ManyPostsResponse(RWModel):
    posts_count: int = Field(...)
    posts: List[PostBase] = Field(...)
