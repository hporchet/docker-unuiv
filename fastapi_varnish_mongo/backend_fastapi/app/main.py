from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware
import logging

from api.api_v1.api import api_router
from core.config import settings
from db.mongodb_utils import connect_to_mongo, close_mongo_connection
from core.errors import http_error_handler, not_found_handler


app = FastAPI(
    title=settings.PROJECT_NAME
)

# Logging configuration
logging.basicConfig(level=logging.DEBUG, format='%(levelname)s(%(name)s): %(message)s')
logger = logging.getLogger(__name__)
logger.info("Starting FastAPI")

logger.debug("Debug mode is on")
logger.error("Error mode is on")
logger.warning("Warning mode is on")

# Set all CORS enabled origins
if settings.BACKEND_CORS_ORIGINS:
    app.add_middleware(
        CORSMiddleware,
        allow_origins=[str(origin) for origin in settings.BACKEND_CORS_ORIGINS],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )


# add the api router
app.include_router(api_router)

# add the event handlers
app.add_event_handler("startup", connect_to_mongo)
app.add_event_handler("shutdown", close_mongo_connection)

# add the error handlers
app.add_exception_handler(404, not_found_handler)
app.add_exception_handler(Exception, http_error_handler)
