from fastapi import APIRouter
from fastapi.responses import JSONResponse

router = APIRouter()


@router.head("/healthcheck")
def healthcheck():
    return JSONResponse(status_code=200, content="OK")
