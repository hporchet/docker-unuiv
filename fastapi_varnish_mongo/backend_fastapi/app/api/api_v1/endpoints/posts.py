from fastapi import APIRouter, Depends
from fastapi.responses import JSONResponse

from api.utils.cache_decorator import cached, ban
from api.utils.response import create_json_response
from core.config import settings
from models.post import ManyPostsResponse
from crud.post import get_all_posts
from db.mongodb import get_database, AsyncIOMotorClient

import logging

logger = logging.getLogger(__name__)

router = APIRouter()


@router.post("/bancache")
@ban(
    cache_host=settings.CACHE_HOST,
    path="/posts",
)
async def ban_cache():
    return JSONResponse(status_code=200, content="Banned cache")


@router.get(
    "/posts",
    response_model=ManyPostsResponse,
    description="Get all posts",
)
@cached(
    max_age=5,
    custom_headers=settings.CACHE_HEADERS,
    public=False,
    prevent_browser_cache=True
)
async def get_posts(db: AsyncIOMotorClient = Depends(get_database)):
    posts = await get_all_posts(db)
    return create_json_response(ManyPostsResponse(posts=posts, posts_count=len(posts)))
