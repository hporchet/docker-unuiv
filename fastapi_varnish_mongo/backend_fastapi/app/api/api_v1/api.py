from fastapi import APIRouter

from .endpoints import posts, stats

api_router = APIRouter()
api_router.include_router(posts.router, prefix="/api/v1")
api_router.include_router(stats.router, prefix="/stats")
