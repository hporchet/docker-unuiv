"""
Cache decorator for API routes.
Using @cached will add the Cache-Control header to the response.
Using @ban will send a fire and forget request to the varnish server.
"""
from functools import wraps, partial
from asyncio import new_event_loop, set_event_loop
from requests import request
from requests.exceptions import RequestException

from fastapi import Response
from starlette.responses import JSONResponse
import logging

logger = logging.getLogger(__name__)


def cached(max_age=300, public=True, custom_headers=None, prevent_browser_cache=False):
    """
    Cache decorator for API routes.
    Adds the Cache-Control header to the response.
    See https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Cache-Control
    Varnish will use this header to cache the response.
    See https://varnish-cache.org/docs/6.0/reference/vcl.html#cache-control
    :param max_age: The max age of the cache in seconds
    :param public: If the cache is public or not
    :param custom_headers: A dictionary of custom headers to add to the response
    :param prevent_browser_cache: If the browser should cache the response
    """

    def decorator(func):
        @wraps(func)
        async def wrapper(*args, **kwargs):
            result = await func(*args, **kwargs)
            # Add the custom headers to the response
            if isinstance(result, JSONResponse):
                result = add_headers_to_response(result, max_age, public, custom_headers, prevent_browser_cache)
            elif isinstance(result, Response):
                result = add_headers_to_response(result, max_age, public, custom_headers, prevent_browser_cache)
            else:
                # Check that the kwargs contains the response object
                # parse the kwargs to get the object with the correct type (Response)
                response_key = None
                for key, value in kwargs.items():
                    if isinstance(value, Response):
                        response_key = key
                        break
                if response_key is None:
                    raise ValueError('The response object was not found in the kwargs, '
                                     'the @cached decorator can only be used on routes that specify a Response object '
                                     'in their parameters.')
                # Add the Cache-Control header to the response
                if public:
                    kwargs[response_key].headers['Cache-Control'] = f'public, max-age={max_age}'
                else:
                    kwargs[response_key].headers['Cache-Control'] = f'max-age={max_age}'
                if prevent_browser_cache:
                    kwargs[response_key].headers['Pragma'] = 'no-cache'
                    kwargs[response_key].headers['Expires'] = '0'
                if custom_headers:
                    for header in custom_headers:
                        kwargs[response_key].headers[header] = custom_headers[header]
            return result

        return wrapper

    return decorator


def ban(method='BAN', cache_host='http://example.org', headers=None, path=''):
    """
    Ban decorator for API routes.
    Sends a request to the varnish server with a BAN request.
    See https://varnish-cache.org/docs/6.0/reference/vcl.html#ban
    :param method: The request method
    :param cache_host: The host of the cache server
    :param headers: The headers of the request
    :param path: The route of the request
    """

    def decorator(func):
        @wraps(func)
        async def wrapper(*args, **kwargs):
            route = ''
            response = await func(*args, **kwargs)
            try:
                # Search for parameters in the route, e.g. <id>
                params = [param for param in path.split('/') if param.startswith('{')]
                logger.debug(f'Params: {params}')
                # and replace them with the actual values
                for param in params:
                    route = path.replace(param, kwargs[param[1:-1]])
                logger.debug(f'Route: {route}')
            except KeyError as ex:
                logger.error(f'Could not find the parameter {param}')
                raise KeyError(f'The parameter {param} is not in the route, perhaps it is misspelled?') from ex
            # Send the request to the cache server
            send_request(method, cache_host, headers=headers, route=route)
            return response

        return wrapper

    return decorator


def fire_and_forget(func):
    """
    Decorator to run a function in the background.
    """

    @wraps(func)
    def wrapper(*args, **kwargs):
        if callable(func):
            logger.debug(f'Running {func.__name__} in the background')
            loop = new_event_loop()
            set_event_loop(loop)
            loop.run_in_executor(None, partial(func, *args, **kwargs))
        else:
            raise Exception('The function is not callable')

    return wrapper


def add_headers_to_response(response_obj, max_age, public, custom_headers, prevent_browser_cache):
    # Add the Cache-Control header to the response
    if public:
        response_obj.headers['Cache-Control'] = f'public, max-age={max_age}'
    else:
        response_obj.headers['Cache-Control'] = f'max-age={max_age}'
    if prevent_browser_cache:
        response_obj.headers['Pragma'] = 'no-cache'
        response_obj.headers['Expires'] = '0'
    if custom_headers:
        for header in custom_headers:
            response_obj.headers[header] = custom_headers[header]

    return response_obj


@fire_and_forget
def send_request(method, host, headers=None, route='', timeout=1):
    """
    Send a request to a given host.
    :param method: The request method
    :param host: The host of the server
    :param headers: The headers of the request
    :param route: The route of the request
    :param timeout: The timeout of the request
    """
    url = f'{host}{route}'
    logger.debug(f'Sending a {method} request to {url}')
    try:
        res = request(method=method, url=url, headers=headers, timeout=timeout)
        logger.debug(f'Response code from {url}: {res.status_code}')
    except RequestException as ex:
        logger.error(f'Error while sending a {method} request to {url}: {ex}')
