from fastapi.encoders import jsonable_encoder
from pydantic import BaseModel
from starlette.responses import JSONResponse


def create_json_response(model: BaseModel) -> JSONResponse:
    """
    Create a JSON response from a model.
    :param model: The model
    :return: The JSON response
    """
    return JSONResponse(content=jsonable_encoder(model))
