from typing import List, Union
from os import environ

from pydantic import AnyHttpUrl, BaseSettings, validator, AnyUrl


class Settings(BaseSettings):
    LOG_LEVEL: str = environ.get("API_LOG_LEVEL", "INFO")
    SERVER_NAME: str = environ.get("API_NAME", "api")
    SERVER_HOST: AnyHttpUrl = f'http://{environ.get("API_HOST", "localhost")}'
    SERVER_PORT: int = environ.get("API_PORT", 8000)
    DEBUG_MODE: bool = environ.get("DEBUG_MODE", False)
    PROJECT_NAME: str = environ.get("PROJECT_NAME", "FastAPI")

    # Varnish
    CACHE_HOST: AnyHttpUrl = f'http://{environ.get("VARNISH_HOST", "varnish")}'
    CACHE_HEADERS: dict = {"X-Cache-Control": "cacheable"}
    # BACKEND_CORS_ORIGINS is a JSON-formatted list of origins
    # e.g: '["http://localhost", "http://localhost:4200", "http://localhost:3000", \
    # "http://localhost:8080", "http://local.dockertoolbox.tiangolo.com"]'
    BACKEND_CORS_ORIGINS: List[AnyHttpUrl] = []

    @validator("BACKEND_CORS_ORIGINS", pre=True)
    def assemble_cors_origins(cls, v: Union[str, List[str]]) -> Union[List[str], str]:
        if isinstance(v, str) and not v.startswith("["):
            return [i.strip() for i in v.split(",")]
        elif isinstance(v, (list, str)):
            return v
        raise ValueError(v)

    MONGO_URL: str = environ.get("MONGODB_URL", "")

    MONGO_DB: str = environ.get("MONGO_DB", "db")
    if not MONGO_URL:
        MONGO_HOST: str = environ.get("MONGO_HOST", "localhost")
        MONGO_PORT: int = int(environ.get("MONGO_PORT", 27017))
        MONGO_USER: str = environ.get("MONGO_USER", "userdb")
        MONGO_PASS: str = environ.get("MONGO_PASSWORD", "pass")
        MONGO_URL: AnyUrl = f"mongodb://{MONGO_USER}:{MONGO_PASS}@{MONGO_HOST}:{MONGO_PORT}"

    MAX_CONNECTIONS_COUNT: int = int(environ.get("MAX_CONNECTIONS_COUNT", 100))
    MIN_CONNECTIONS_COUNT: int = int(environ.get("MIN_CONNECTIONS_COUNT", 10))

    post_collection: str = 'post'
    database_name: str = MONGO_DB


settings = Settings()
