from starlette.exceptions import HTTPException
from starlette.requests import Request
from starlette.responses import JSONResponse


async def http_error_handler(request: Request, exc: Exception) -> JSONResponse:
    """
    Custom HTTP error handler.
    :param request: The request object
    :param exc: The exception
    :return: A JSON response with the error message
    """
    if isinstance(exc, HTTPException):
        return JSONResponse(
            status_code=exc.status_code,
            content={
                "status": "error",
                "detail": exc.detail
            }
        )
    else:
        # Extract the error message from the exception
        error_message = str(exc)
        return JSONResponse(
            status_code=500,
            content={
                "status": "error",
                "detail": error_message
            }
        )


async def not_found_handler(request: Request, exc: Exception) -> JSONResponse:
    """
    Custom not found error handler.
    :param request: The request object
    :param exc: The exception
    :return: A JSON response with the error message
    """
    return JSONResponse(
        status_code=404,
        content={
            "status": "error",
            "detail": "We could not find the resource you were looking for."
        }
    )
